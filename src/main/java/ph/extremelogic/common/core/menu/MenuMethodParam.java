package ph.extremelogic.common.core.menu;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since July 30, 2017
 */

/**
 * Class that handles the method parameters.
 * 
 * @author Jan So
 * @since July 30, 2017
 *
 * @param <T> Type of the object.
 */
public class MenuMethodParam<T> {
    private Object parameter;
    private Class<T> parameterType;
    private String inputDisplay;

    /**
     * @return parameter Object parameter.
     */
    public Object getParameter() {
        return parameter;
    }

    /**
     * @param parameter the parameter to set.
     */
    public void setParameter(Object parameter) {
        this.parameter = parameter;
    }

    /**
     * @return parameterType Type of the parameter.
     */
    public Class<T> getParameterType() {
        return parameterType;
    }

    /**
     * @param parameterType the parameterType to set.
     */
    public void setParameterType(Class<T> parameterType) {
        this.parameterType = parameterType;
    }

    /**
     * @return inputDisplay Display prompt for variable substitution.
     */
    public String getInputDisplay() {
        return inputDisplay;
    }

    /**
     * @param inputDisplay the inputDisplay to set.
     */
    public void setInputDisplay(String inputDisplay) {
        this.inputDisplay = inputDisplay;
    }
}

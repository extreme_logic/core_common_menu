package ph.extremelogic.common.core.menu;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since July 20, 2017
 */

import java.util.ArrayList;

/**
 * Menu class.
 *
 * @author Jan So
 * @since July 20, 2017
 *
 */
public class Menu {
    /**
     * Menu key value.
     */
    private String key;

    /**
     * Menu display name.
     */
    private String displayName;

    /**
     * Grouping of the menu.
     */
    private String module;

    /**
     * Method to invoke if key matched.
     */
    private String method;

    /**
     * Method module exit point.
     */
    private String moduleExit;

    /**
     * List of parameters with various type related information.
     */
    private ArrayList<MenuMethodParam<?>> parameters;

    /**
     * @return key The menu identifier.
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return displayName Display to be displayed in the menu.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set.
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return module Grouping of the menu.
     */
    public String getModule() {
        return module;
    }

    /**
     * @param module the module to set.
     */
    public void setModule(String module) {
        this.module = module;
    }

    /**
     * @return method The method to be invoked.
     */
    public String getMethod() {
        return method;
    }

    /**
     * @param method the method to set.
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * @return Parameter information.
     */
    public ArrayList<MenuMethodParam<?>> getParameters() {
        return parameters;
    }

    /**
     * @param parameters the parameters to set.
     */
    public void setParameters(ArrayList<MenuMethodParam<?>> parameters) {
        this.parameters = parameters;
    }

    /**
     * @return moduleExit String to identify the exist status.
     */
    public String getModuleExit() {
        return moduleExit;
    }

    /**
     * @param moduleExit the moduleExit to set.
     */
    public void setModuleExit(String moduleExit) {
        this.moduleExit = moduleExit;
    }
}

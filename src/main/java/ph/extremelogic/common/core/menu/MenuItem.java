package ph.extremelogic.common.core.menu;

/*
 * Copyright (C) 2018 Extreme Logic Ph.
 * https://www.extremelogic.ph
 * 
 * Distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied.
 *
 * @since July 30, 2017
 */

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Class that handles menu items.
 * 
 * @author Jan So
 * @since July 30, 2017
 *
 * @param <T> Type of the menu item.
 */
public class MenuItem<T> {
    private HashMap<String, Menu> hm;
    private HashMap<String, ArrayList<MenuMethodParam<?>>> param;

    private String exitKey;
    private String heading;

    private List<Menu> list;

    /**
     * Default menu entry.
     */
    private static final String DEFAULT_MODULE_ENTRY = "_MENU_ENTRY_";

    public MenuItem() {
        hm = new HashMap<String, Menu>();
        param = new HashMap<String, ArrayList<MenuMethodParam<?>>>();
    }

    public MenuItem(String heading) {
        this();
        this.heading = heading;
    }

    /**
     * @param key Key binding for the menu.
     * @param displayName Display for the menu.
     * @param module Grouping of the menu.
     */
    public void addExit(String key, String displayName, String module) {
        exitKey = key;
        Menu menu = new Menu();
        menu.setDisplayName(displayName);
        menu.setKey(key);
        menu.setModule(null == module ? DEFAULT_MODULE_ENTRY : module);
        hm.put(key, menu);
    }

    /**
     * @param method The method to be called.
     * @param value The value to be passed to the method.
     * @param parameterType The data type to be passed for this parameter.
     */
    public <X> void addParameter(String method, Object value,
            Class<X> parameterType) {
        addParameter(method, value, parameterType, null);
    }

    /**
     * Adds parameter for the method to be called.
     *
     * @param method Method name to be populated with parameters.
     * @param value Value to be passed as a parameter.
     * @param parameterType Data type of the parameter.
     */
    public <X> void addParameter(String method, Object value,
            Class<X> parameterType, String inputDisplay) {
        ArrayList<MenuMethodParam<?>> a;
        if (!param.containsKey(method)) {
            MenuMethodParam<X> m = new MenuMethodParam<X>();
            m.setParameter(value);
            m.setParameterType(parameterType);
            m.setInputDisplay(inputDisplay);

            a = new ArrayList<MenuMethodParam<?>>();

            a.add(m);
            param.put(method, a);
        } else {
            a = param.get(method);

            MenuMethodParam<X> m = new MenuMethodParam<X>();
            m.setParameter(value);
            m.setParameterType(parameterType);
            m.setInputDisplay(inputDisplay);
            a.add(m);
            param.put(method, a);
        }

        Iterator<Map.Entry<String, Menu>> it = hm.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Menu> pair = it.next();
            Menu menu = (Menu) pair.getValue();
            if (method.equalsIgnoreCase(menu.getMethod())) {
                String key = menu.getKey();
                menu.setParameters(a);
                hm.replace(key, menu);
            }
        }
    }

    /**
     * Adds a menu entry.
     *
     * @param key Key for the menu.
     * @param displayName - Display item name.
     * @param method - Method to be called if this menu entry is selected.
     * @param moduleEntry - Module name or grouping of menu items entry.
     * @param moduleExit - Module name or grouping of menu items exit.
     * @return Parameters for the method.
     */
    public ArrayList<MenuMethodParam<?>> addMenu(String key, String displayName,
            String method, String moduleEntry, String moduleExit) {
        Menu menu = new Menu();

        menu.setDisplayName(displayName);
        menu.setKey(key);
        menu.setModule(
                null == moduleEntry ? DEFAULT_MODULE_ENTRY : moduleEntry);
        menu.setMethod(method);
        menu.setModuleExit(
                null == moduleExit ? DEFAULT_MODULE_ENTRY : moduleExit);

        hm.put(key, menu);

        return new ArrayList<MenuMethodParam<?>>();
    }

    /**
     * Use a value to its appropriate class.
     * 
     * @param clazz Class type of the value.
     * @param value Value of the object.
     * @return <code>Object</code> value using the original class.
     */
    private Object toObject(Class<?> clazz, String value) {
        if (Boolean.class == clazz) {
            return Boolean.parseBoolean(value);
        } else if (Byte.class == clazz) {
            return Byte.parseByte(value);
        } else if (Short.class == clazz) {
            return Short.parseShort(value);
        } else if (Integer.class == clazz) {
            return Integer.parseInt(value);
        } else if (Long.class == clazz) {
            return Long.parseLong(value);
        } else if (Float.class == clazz) {
            return Float.parseFloat(value);
        } else if (Double.class == clazz) {
            return Double.parseDouble(value);
        } else if (int.class == clazz) {
            return Integer.parseInt(value);
        } else if (float.class == clazz) {
            return Float.parseFloat(value);
        }
        return value;
    }

    /**
     * @param input User key entry or selection.
     * @param console instance class that contains the methods to be invoked.
     * @throws Exception thrown if there is a violation.
     */
    private Object processSelection(String input, T console, Scanner scanner)
            throws Exception {
        Method m;
        String methodName = "";
        String moduleExit = DEFAULT_MODULE_ENTRY;
        Object ret = null;

        if (null == list || list.isEmpty()) {
            throw new Exception("No menu items specified.");
        }

        try {
            for (Menu men : list) {
                if (men.getKey().equalsIgnoreCase(input)) {
                    methodName = men.getMethod();
                    moduleExit = men.getModuleExit();
                    if (null != methodName && !methodName.isEmpty()) {
                        if (null == men.getParameters()
                                || men.getParameters().isEmpty()) {
                            m = console.getClass().getMethod(methodName);
                            ret = m.invoke(console);
                        } else {
                            int size = men.getParameters().size();

                            Class<?>[] paramTypes = new Class[size];
                            Object[] parameters = new Object[size];

                            int index = 0;
                            for (MenuMethodParam<?> mmp : men.getParameters()) {
                                Object obj = mmp.getParameter();
                                if (null == obj) {
                                    System.out.print(mmp.getInputDisplay());
                                    obj = scanner.next();
                                    obj = toObject(mmp.getParameterType(),
                                            (String) obj);
                                }
                                parameters[index] = obj; // mmp.getParameterType().cast(obj);
                                paramTypes[index] = mmp.getParameterType();
                                index++;
                            }
                            m = console.getClass().getMethod(methodName,
                                    paramTypes);
                            ret = m.invoke(console, parameters);
                        }
                    }
                    break;
                } else {
                    methodName = "";
                    moduleExit = DEFAULT_MODULE_ENTRY;
                }
            }
        } catch (ReflectiveOperationException e) {
            String message;
            // e.printStackTrace();
            Throwable cause = e.getCause();
            if(null != cause) {
                message = cause.getLocalizedMessage();
            } else {
                message = e.getLocalizedMessage();
            }
            throw new Exception(message);
        }
        list = getMenu(moduleExit);
        return ret;
    }

    /**
     * Displays the menu for user entry processing.
     *
     * @param module Menu grouping or module to be displayed.
     * @param console Contains instance of methods to be invoked.
     * @throws Exception Thrown if there is a violation of use.
     */
    public void processMenu(String module, T console, boolean handleException)
            throws Exception {
        list = getMenu(null == module ? DEFAULT_MODULE_ENTRY : module);

        String str;
        Scanner keyboard = new Scanner(System.in);

        do {
            System.out.println(heading);
            StringBuilder sb = new StringBuilder();
            for (Menu m : list) {
                sb.append(String.format("[%s] %s  |  ", m.getKey(), m.getDisplayName()));
            }
            String menu = sb.toString().trim();
            int fieldSize = menu.length();
            System.out.println(String.format("%"+ fieldSize + "s", "").replace(' ', '='));
            System.out.format(menu);
            System.out.println();
            System.out.println(String.format("%"+ fieldSize + "s", "").replace(' ', '='));
            System.out.println();
            System.out.print("\rCommand: ");
            str = keyboard.next();

            if (str.equalsIgnoreCase(exitKey)) {
                break;
            } else {
                try {
                    processSelection(str, console, keyboard);
                } catch (Exception e) {
                    if (handleException) {
                        System.err.println(
                                "\rException: " + e.getLocalizedMessage());
                    } else {
                        throw e;
                    }
                }
            }
        } while (true);
        keyboard.close();
    }

    /**
     * Retrieves menu entry list.
     *
     * @param module Module or grouping to be displayed.
     * @return Menu entries.
     */
    private List<Menu> getMenu(String module) {
        ArrayList<Menu> list = new ArrayList<Menu>();

        Iterator<Map.Entry<String, Menu>> it = hm.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Menu> pair = it.next();
            Menu menu = (Menu) pair.getValue();
            if (module.equalsIgnoreCase(menu.getModule())) {
                list.add(menu);
            }
        }
        return list;
    }
}

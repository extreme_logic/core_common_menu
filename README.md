# Extreme Logic PH Common Menu

A common library that provides the following
 - Cpnsole Menu

## Maven repository
To use in your project, include this as a dependency in your project.
```
<!-- https://mvnrepository.com/artifact/ph.extremelogic/extremelogic-common-menu -->
<dependency>
    <groupId>ph.extremelogic</groupId>
    <artifactId>extremelogic-common-menu</artifactId>
    <version>0.0.1</version>
</dependency>
```

## Deploy to Maven Repository

```
mvn clean deploy
```

With the property autoReleaseAfterClose set to false you can manually inspect the staging repository in the Nexus Repository Manager and trigger a release of the staging repository later with

```
mvn nexus-staging:release
```

If you find something went wrong you can drop the staging repository with

```
mvn nexus-staging:drop
```

